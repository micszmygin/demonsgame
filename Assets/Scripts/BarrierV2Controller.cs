﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BarrierV2Controller : MonoBehaviour {
    List<Rune> runes;
    public float hp = 10;
    public bool demonInBarrier = false;

    private void Awake() {
        runes = new List<Rune>();
    }

    private void Update() {
        if (demonInBarrier) {
            hp -= Time.deltaTime;
        }

        if (hp <= 0) {
            foreach(Rune rune in runes) {
                Destroy(rune.gameObject);
            }
            Destroy(gameObject);
        }
    }

    public void AddRune(Rune rune) {
        runes.Add(rune);
    }

    private void OnTriggerStay(Collider coll) {
        AI enemy = coll.GetComponent<AI>();

        if (enemy != null) {
            Debug.Log("STAYED");
            enemy.BarrierInteract(gameObject);
            demonInBarrier = true;
        }
    }

    private void OnTriggerExit(Collider coll) {
        AI enemy = coll.GetComponent<AI>();

        if (enemy != null) {
            Debug.Log("EXITED");
            enemy.BarrierEscaped();
            demonInBarrier = false;
        }
    }
}
