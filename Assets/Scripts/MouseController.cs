﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseController : MonoBehaviour {
    public GameObject rune;
    public GameObject barrier;
    private GameObject actualBarrier;
    private GameObject runeInstance;
    private Vector3 mouseWorldPos;
    
    void Update () {

        UpdateMouse();

        TryShowLine();

        if (Input.GetMouseButtonDown(0) && MaxRunesCreated()) {

            DrawLine();

            CreateRune();

            BarrierUpdate();
        }
	}

    bool MaxRunesCreated() {
        return Rune.COUNT < GameController.MAX_RUNES;
    }

    void BarrierUpdate() {
        actualBarrier.GetComponent<LineRenderer>().SetPosition(GetTriangleVert(), runeInstance.transform.position);
        actualBarrier.GetComponent<BarrierV2Controller>().AddRune(runeInstance.GetComponent<Rune>());
    }

    void CreateRune() {
        runeInstance = Instantiate(rune);
        runeInstance.transform.position = new Vector3(mouseWorldPos.x, -0.05f, mouseWorldPos.z);
    }

    void UpdateMouse() {
        mouseWorldPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        transform.position = new Vector3(mouseWorldPos.x, transform.position.y, mouseWorldPos.z);
    }

    void TryShowLine() {
        if (PutNewLine() && actualBarrier != null)
            actualBarrier.GetComponent<DrawLine>().isEnded = true;
    }

    void DrawLine() {
        if (PutNewLine()) {
            actualBarrier = Instantiate(barrier);
        }
    }

    bool PutNewLine() {
        return Rune.COUNT % 3 == 0;
    }

    int GetTriangleVert() {
        return Rune.COUNT % 3;
    }
}
