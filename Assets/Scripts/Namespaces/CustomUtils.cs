﻿using UnityEngine;
using System.Collections;

namespace Custom {

    public class Utils : MonoBehaviour {

        public static Transform FindNearestObject(string tagType, bool ignoreMe, Transform startPos) {

            RaycastHit2D[] colls = Physics2D.CircleCastAll(startPos.position, 10f, Vector2.zero);

            float distance = Mathf.Infinity;

            Transform nearestTransform = null;

            foreach (RaycastHit2D coll in colls) {
                float collDistance = Vector2.Distance(coll.transform.position, startPos.position);

                if (ignoreMe && coll.transform.gameObject.Equals(startPos.gameObject))
                    continue;

                if (coll.transform != null && coll.transform.name == tagType && collDistance < distance) {
                    distance = collDistance;

                    nearestTransform = coll.transform;
                }
            }

            return nearestTransform;

        }
    }
}
