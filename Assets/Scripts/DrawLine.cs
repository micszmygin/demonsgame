﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrawLine : MonoBehaviour {
    LineRenderer lineRenderer;  
    public bool isEnded = false;

    void Start () {
        lineRenderer = GetComponent<LineRenderer>();
        lineRenderer.enabled = false;
	}

	void Update () {
        if (isEnded) {
            lineRenderer.enabled = true;
        }
    }
}
