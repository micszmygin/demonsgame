﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameController : MonoBehaviour {
    public static int MAX_RUNES = 6;
    public static bool IS_NIGHT = false;

    private bool activeFlag = false;

    Text RuneText;

    private void Start() {
        RuneText = GetComponentInChildren<Text>();    
    }

    void Update () {
        if (Input.GetKeyDown(KeyCode.Alpha1)) {
            activeFlag = !activeFlag;
            ActivateEnemy(activeFlag);           
        }

        if (Input.GetKeyDown(KeyCode.R))
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);

        DrawRuneText();
    }

    void DrawRuneText() {
        RuneText.text = "rune count: " + Rune.COUNT.ToString() + " / " + MAX_RUNES.ToString();
    }

    void ActivateEnemy(bool activate) {
        AI[] enemies = FindObjectsOfType<AI>();

        foreach (AI enemy in enemies) {
            enemy.isActivated = activate;
        }
    }
}
