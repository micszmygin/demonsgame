﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {
    public Transform target;

	void Update () {
        if(target)
            transform.position = new Vector3(target.position.x, transform.position.y, target.position.z);
	}
}
