﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControl : MonoBehaviour {
    Rigidbody rb;
    public float speed = 10f;
    public float maxDashTime = 0.5f;
    public float dashPower = 2f;

    float timer = 0f;

	// Use this for initialization
	void Start () {
        rb = GetComponent<Rigidbody>();
	}

    private void Update() {
        if (Input.GetButtonDown("Dash") && timer==0)
            timer = maxDashTime;

        if (timer > 0)
            timer -= Time.deltaTime;
        else
            timer = 0;
    }

    // Update is called once per frame
    void FixedUpdate () {
        Vector3 vecMove;

        float moveX = Input.GetAxis("Horizontal") * Time.deltaTime * speed;
        float moveZ = Input.GetAxis("Vertical") * Time.deltaTime * speed;

        vecMove = new Vector3(moveX, rb.velocity.y, moveZ);

        if(timer!=0) vecMove = new Vector3(moveX * dashPower, rb.velocity.y, moveZ * dashPower);

        rb.velocity = vecMove;
	}


}
