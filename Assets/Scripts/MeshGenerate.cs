﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MeshFilter))]

public class MeshGenerate : MonoBehaviour {
    LineRenderer lineRenderer;
    DrawLine drawLine;
    bool isTriangleClosed = false;
    Mesh mesh;   
    Vector3[] vertices;
    int[] triangles;

    private void Awake() {
        mesh = GetComponent<MeshFilter>().mesh;
        lineRenderer = GetComponent<LineRenderer>();
        drawLine = GetComponent<DrawLine>();
    }

    private void Update() {
        if(lineRenderer.enabled && !isTriangleClosed) {
            isTriangleClosed = true;
            CreateMesh();
        }
    }

    void TriangleData() {
        Vector3 verticleHeight = new Vector3(0, 4f, 0f);

        Vector3 p0 = lineRenderer.GetPosition(0);
        Vector3 p1 = lineRenderer.GetPosition(1);
        Vector3 p2 = lineRenderer.GetPosition(2);
        Vector3 p3 = lineRenderer.GetPosition(0) + verticleHeight;
        Vector3 p4 = lineRenderer.GetPosition(1) + verticleHeight;
        Vector3 p5 = lineRenderer.GetPosition(2) + verticleHeight;

        vertices = new Vector3[] {
            p0,p1,p2,
            p3,p4,p5
        };

        triangles = new int[] {
            0,1,2,
            0,1,4,
            4,3,0,
            1,4,5,
            5,2,1,
            2,5,0,
            3,5,2,
            3,4,5,
            5,4,3,
        };
    }

    void CreateMesh() {
        TriangleData();

        mesh.Clear();

        mesh.vertices = vertices;
        mesh.triangles = triangles;

        MeshCollider mc = gameObject.AddComponent<MeshCollider>();
        mc.sharedMesh = mesh;
        mc.convex = true;
        mc.isTrigger = true;
        
    }
}
