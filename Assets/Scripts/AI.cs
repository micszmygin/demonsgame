﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AI : MonoBehaviour {
    public bool isActivated = false;
    public float hp = 10;
    public float speed = 2;
    public Transform target;   
    public Material rockMaterial;
    public Material defaultMaterial;

    private GameObject barrierCached;
    private NavMeshAgent navMesh;
    private Renderer render;
    private ParticleSystem partSystem;

    private void Start() {
        partSystem = GetComponent<ParticleSystem>();
        navMesh = GetComponent<NavMeshAgent>();
        render = GetComponent<Renderer>();
        navMesh.speed = speed;
        barrierCached = gameObject; // po to by nie wywalało odrazu
    }

    public void BarrierInteract(GameObject go) {
        partSystem.Emit(2);
        barrierCached = go;
        hp -= Time.deltaTime;
        navMesh.speed = speed / 5;
    }

    public void BarrierEscaped() {
        navMesh.speed = speed;
    }

    void Update () {
        if (isActivated) {
            render.material = defaultMaterial;
            navMesh.isStopped = false;
            if (target)
                navMesh.SetDestination(target.position);
        }
        else {
            render.material = rockMaterial;
            navMesh.isStopped = true;
        }

        if (barrierCached == null)
            BarrierEscaped();

        if (hp <= 0) {
            Destroy(gameObject);
        }
    }
}
